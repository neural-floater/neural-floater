#include <Arduino.h>

const int escPin = 18; // 选择一个PWM引脚
const int escPin2 = 4; // 选择一个PWM引脚
const int pwmChannel = 0; // 使用的PWM通道
const int pwmChannel2 = 1; // 使用的PWM通道
const int pwmFrequency = 50; // PWM信号频率 (Hz)
const int pwmResolution = 16; // PWM分辨率 (位)
const int minPulseWidth = 1000; // 最小脉冲宽度 (微秒)
const int maxPulseWidth = 2000; // 最大脉冲宽度 (微秒)
int inputNumber ;
void setup() {
  Serial.begin(115200); // 初始化串口通信
  //Serial.println("初始化开始");
  ledcSetup(pwmChannel, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(escPin, pwmChannel); // 将PWM通道附加到指定引脚

  ledcSetup(pwmChannel2, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(escPin2, pwmChannel2); // 将PWM通道附加到指定引脚

  ledcWrite(pwmChannel2, 4500);//激活电机...
  ledcWrite(pwmChannel, 4500);
  delay(2000);
  Serial.println("初始化完成");
}

void loop() {
  for (int i = 1000; i < 9000; i = i + 500) {
    ledcWrite(pwmChannel2, i);
    ledcWrite(pwmChannel, i);
    Serial.printf("当前数字：");
    Serial.println(i,DEC);
    delay(2000);
  }
  
  // if (Serial.available() > 0 ) {
  //   // 读取串口输入
  //   inputNumber = Serial.parseInt();
  //   if(inputNumber != 0){
  //     //ledcWrite(pwmChannel, inputNumber);
  //     ledcWrite(pwmChannel2, inputNumber);
  //     Serial.println(inputNumber,DEC);
  //     delay(5000);
  //   }
  // }

}// <= 4600, nag;   >= 1500 
//  >= 5250, pos;   <= 8350
