#ifndef __PID_H
#define __PID_H

class PID{
public:
	float Kp;	// 比例增益
	float Ki;	// 积分增益
	float Kd;	// 微分增益

	float integrator;			// 积分器:存储误差随时间的累计值
	float prevError;			// 前一次误差

	float out;	// 输出

	// 进行PID控制
	void PIDUpdate(float setpoint, float measurement);
};

#endif