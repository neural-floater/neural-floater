#include "PID.h"

// 进行PID控制
void PID::PIDUpdate(float setpoint, float measurement) {
    // 计算误差
    float error = setpoint - measurement;
    // 计算 P
    float proportional = Kp * error;
    // 计算 I
    integrator += Ki * (error + prevError);
    // 计算 D
    float differentiator = Kd * (error - prevError);
    // 计算输出
    out = proportional + integrator + differentiator;
    // 保存值
    prevError = error;
}
