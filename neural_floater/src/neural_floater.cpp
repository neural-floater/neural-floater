#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "controller.h"
#include "esp32.h"

// 当前位姿信息
Eigen::MatrixXf currentPos(1,3);
Eigen::MatrixXf currentAngle(1,3);

Controller controller;
ESP32 esp32;

ros::Subscriber pose_subscriber;


void poseCallback(const geometry_msgs::PoseStamped::ConstPtr& msg)
{
    // 从动捕设备获取飞艇当前位置
    currentPos(0,0) = msg->pose.position.x;
    currentPos(0,1) = msg->pose.position.y;
    currentPos(0,2) = msg->pose.position.z;
    toEulerAngle(msg->pose.orientation.x, msg->pose.orientation.y, msg->pose.orientation.z, msg->pose.orientation.w, currentAngle);
    controller.getPWM(currentPos, currentAngle);
    // 计算 6 轴力及 pwm
    Eigen::VectorXf pwm = controller.pwm(6);
    esp32.send(pwm);
}

int main(int argc, char** argv)
{
    ros::init(argc, argv, "neural_floater");
    ros::NodeHandle nh;

    controller.init();
    esp32.init();

    pose_subscriber = nh.subscribe("/vrpn_client_node/blimp/pose", 10, poseCallback);

    ros::spin();

    return 0;
}
