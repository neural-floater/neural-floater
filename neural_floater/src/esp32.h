#ifndef __ESP32_H
#define __ESP32_H

#include <iostream>
#include <cstring>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <Eigen/Dense>
#include "func.h"

class ESP32{
public:
    const char* server_ip = "10.1.1.32"; // 服务器IP地址
    int port = 80;
    int sockfd;

    void init();
    void send(Eigen::VectorXf pwm);
};

#endif