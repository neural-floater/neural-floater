#ifndef __FUNC_H
#define __FUNC_H

#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include <string>

void toEulerAngle(const double x,const double y,const double z,const double w, Eigen::MatrixXf& currentAngle);
Eigen::Matrix3f getRotationMatrix(Eigen::MatrixXf& matrix);
std::string concatenateVector(const Eigen::VectorXf& vec);
char* vectorToStringChar(const Eigen::VectorXf& vec);

#endif