#include "controller.h"
#include "func.h"

void Controller::init(){
    // PID控制参数
    XPosPID =  {2.0f, 0.5f, 0.25f,
                0.0f, 0.0f,
                0.0f
                };
    YPosPID =  {2.0f, 0.5f, 0.25f,
                0.0f, 0.0f,
                0.0f
                };
    ZPosPID =  {2.0f, 0.5f, 0.25f,
                0.0f, 0.0f,
                0.0f
                };
    anglePID = {2.0f, 0.5f, 0.25f,	// 只考虑偏航角
                0.0f, 0.0f,
                0.0f
                };

    // 设置目标点
    targetPos(0,0) = 1.0f;
    targetPos(0,1) = 1.0f;
    targetPos(0,2) = 1.0f;
    targetAngle(0,0) = 0.0f;
    targetAngle(0,1) = 0.0f;
    targetAngle(0,2) = 1.0f;

    k_of_pwm = MaxForce / pow(range_of_pwm,2);
}

void Controller::getForces(Eigen::MatrixXf currentPos, Eigen::MatrixXf currentAngle){
    
    // 获取旋转矩阵
    Eigen::Matrix3f R = getRotationMatrix(currentAngle);

    // 进行PID控制
	XPosPID.PIDUpdate(targetPos(0,0), currentPos(0,0));
	YPosPID.PIDUpdate(targetPos(0,1), currentPos(0,1));
	ZPosPID.PIDUpdate(targetPos(0,2), currentPos(0,2));
	anglePID.PIDUpdate(targetAngle(0,2), currentAngle(0,2));

    // cout << "XPosPID.out is " << XPosPID.out << endl;
	// cout << "YPosPID.out is " << YPosPID.out << endl;
	// cout << "ZPosPID.out is " << ZPosPID.out << endl;
	// cout << "AnglePID.out is " << anglePID.out << endl;

	// 计算世界系下的力 Fw
	Eigen::Vector3f Fw;
	Fw << 	m * XPosPID.out,
			m * YPosPID.out,
			m * ZPosPID.out - Tw + Gw;

	// 求解机体系下的力 Fb
	if (R.determinant() == 0) { // 检查R是否可逆
        cout << "Matrix R is not invertible." << endl;
        return -1;
    }
	Eigen::Matrix3f Rinv = R.inverse();
	Eigen::Vector3f Fb = Rinv * Fw;

    // 定义6x6矩阵和6x1向量用于求解力的分配
    Eigen::MatrixXf A(6, 6);
    Eigen::VectorXf B(6);
	A << 	1,  -1,  -1,   1,  0,  0,
			1,   1,  -1,  -1,  0,  0,
			0,   0,   0,   0,  1,  1,
			1,   1,   1,   1,  0,  0,
			1,  -1,   1,  -1,  0,  0,
			0,   0,   0,   0,  1, -1;

	B << 	1.414213 * Fb(0),
			1.414213 * Fb(1),
			1.0f * Fb(2),
			0.4 * me * r * anglePID.out,
			0.0f,
			0.0f;

    // 求解Ax = b, x为6个力的分配
    forces = A.colPivHouseholderQr().solve(B);

    // for (int i = 0; i < 6; ++i) {
    //     cout << "x" << i + 1 << " = " << x(i) << endl;
    // }

}

// negative: force = k * (pwm - 1500)^2
// positive: force = k * (pwm - 5250)^2
void Controller::getPWM(Eigen::MatrixXf currentPos, Eigen::MatrixXf currentAngle){
    getForces(currentPos, currentAngle);
    for (int i = 0; i < 6; i ++){
        // 逆时针
        if(forces(i) < 0){
            pwm(i) = sqrt((-forces(i)) / k_of_pwm) + 1500;
        }
        // 顺时针
        else{
            pwm(i) = sqrt(  forces(i)  / k_of_pwm) + 5250;
        }
    }
}