#include "esp32.h"

void ESP32::init(){
    // 1. 创建套接字
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd == -1) {
        std::cerr << "Socket creation failed" << std::endl;
        return ;
    }

    // 2. 设置服务器地址结构
    struct sockaddr_in serv_addr;
    memset(&serv_addr, 0, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(port);

    // 3. 将IP地址转换为网络字节序
    if (inet_pton(AF_INET, server_ip, &serv_addr.sin_addr) <= 0) {
        std::cerr << "Invalid address / Address family not supported" << std::endl;
        return ;
    }

    // 4. 连接到服务器
    if (connect(sockfd, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        std::cerr << "Connection failed" << std::endl;
        return ;
    }
}

void ESP32::send(Eigen::VectorXf pwm){
    char* message = vectorToStringChar(pwm);
    if (send(sockfd, message, strlen(message), 0) < 0) {
        std::cerr << "Send failed" << std::endl;
        return ;
    }
}
