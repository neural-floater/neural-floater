#ifndef __CONTROLLER_H
#define __CONTROLLER_H

#include <Eigen/Dense>
#include <cmath>
#include <iostream>
#include "PID.h"


class Controller{
public:
    // 基础参数
    float m = 5.0f;		// 整体质量
    float Tw = 50.0f; 	// 浮力
    float Gw = 50.0f; 	// 重力
    float me = 1.0f; 	// 气球质量
    float r = 0.5f;		// 浮空器俯视图半径
    double MaxForce;
    double k_of_pwm;
    int range_of_pwm;

    Eigen::VectorXf forces(6);
    Eigen::VectorXf pwm(6);

    // 目标点
    Eigen::MatrixXf targetPos(1,3);
    Eigen::MatrixXf targetAngle(1,3);

    // PID 控制器
    PID XPosPID;
    PID YPosPID;
    PID ZPosPID;
    PID anglePID;

    // 初始化 PID 控制器参数，设置目标点位置
    void init();

    // 计算 6 个力
    void getForces(Eigen::MatrixXf currentPos, Eigen::MatrixXf currentAngle);

    void getPWM(Eigen::MatrixXf currentPos, Eigen::MatrixXf currentAngle);
};

#endif