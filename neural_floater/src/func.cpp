#include "func.h"

/*
输入：x,y,z,w　为四元数
输出：roll，pitch，yaw欧拉角
**/

void toEulerAngle(const double x,const double y,const double z,const double w, Eigen::MatrixXf& currentAngle)
{
    double roll,pitch,yaw;
// roll (x-axis rotation)
    double sinr_cosp = +2.0 * (w * x + y * z);
    double cosr_cosp = +1.0 - 2.0 * (x * x + y * y);
    roll = atan2(sinr_cosp, cosr_cosp);

// pitch (y-axis rotation)
    double sinp = +2.0 * (w * y - z * x);
    if (fabs(sinp) >= 1)
        pitch = copysign(M_PI / 2, sinp); // use 90 degrees if out of range
    else
        pitch = asin(sinp);

// yaw (z-axis rotation)
    double siny_cosp = +2.0 * (w * z + x * y);
    double cosy_cosp = +1.0 - 2.0 * (y * y + z * z);
    yaw = atan2(siny_cosp, cosy_cosp);

    currentAngle(0,0) = roll;
    currentAngle(0,1) = pitch;
    currentAngle(0,2) = yaw;
}

// 该函数用于获得机体系到世界系的坐标转换矩阵
Eigen::Matrix3f getRotationMatrix(Eigen::MatrixXf& matrix) {
    // 理论上浮空器只存在偏航角，省略计算可以如下：
    float yaw = matrix(0,2);
    Eigen::Matrix3f R;
    R << cos(yaw), -sin(yaw), 0,
        sin(yaw), cos(yaw), 0,
        0,0,1;
    return R;
}

std::string concatenateVector(const Eigen::VectorXf& vec) {
    std::ostringstream oss; // 创建字符串流对象
    for (int i = 0; i < vec.size(); ++i) {
        oss << vec[i]; // 将向量元素添加到字符串流中
        if (i < vec.size() - 1) {
            oss << " "; // 在元素之间添加空格，除了最后一个元素
        }
    }
    oss << "\n";
    return oss.str(); // 获取字符串流的字符串表示
}

char* vectorToStringChar(const Eigen::VectorXf& vec) {
    std::string str = concatenateVector(vec);
    char* charStr = new char[str.length() + 1]; // 分配足够的内存，包括终止符
    std::strcpy(charStr, str.c_str()); // 复制字符串
    return charStr; // 返回C风格字符串
}
