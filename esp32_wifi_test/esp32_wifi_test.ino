#include <WiFi.h>
#include <Arduino.h>

const char* ssid = "MERCURY_056F";
const char* password = "";

WiFiServer server(80);

const int p1 = 25; 
const int p2 = 26; 
const int p3 = 32; 
const int p4 = 33; 
const int p5 = 34; 
const int p6 = 35; 

const int c1 = 1; 
const int c2 = 2; 
const int c3 = 3;
const int c4 = 4;
const int c5 = 5;
const int c6 = 6;

const int pwmFrequency = 50; // PWM信号频率 (Hz)
const int pwmResolution = 16; // PWM分辨率 (位)

int f1, f2, f3, f4, f5, f6;

IPAddress local_IP(10,1,1,32);
IPAddress gateway(10,1,1,1);
IPAddress subnet(255,255,255,0);

void setup() {
  
  Serial.begin(115200);
  if(!WiFi.config(local_IP, gateway, subnet)){
    Serial.println("STA failed");
  }

  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(1000);
    Serial.println("正在连接WiFi...");
  }

  Serial.println("连接WiFi成功!");

  // 获取并打印当前 IP 地址
  Serial.print("当前IP地址: ");
  Serial.println(WiFi.localIP());

  server.begin();

  ledcSetup(c1, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(p1, c1); // 将PWM通道附加到指定引脚
  
  ledcSetup(c2, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(p2, c2); // 将PWM通道附加到指定引脚

  ledcSetup(c3, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(p3, c3); // 将PWM通道附加到指定引脚

  ledcSetup(c4, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(p4, c4); // 将PWM通道附加到指定引脚

  ledcSetup(c5, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(p5, c5); // 将PWM通道附加到指定引脚

  ledcSetup(c6, pwmFrequency, pwmResolution); // 初始化PWM信号
  ledcAttachPin(p6, c6); // 将PWM通道附加到指定引脚

  //激活电机...
  ledcWrite(c1, 4500);
  ledcWrite(c2, 4500);
  ledcWrite(c3, 4500);
  ledcWrite(c4, 4500);
  ledcWrite(c5, 4500);
  ledcWrite(c6, 4500);

  delay(2000);
  Serial.println("初始化完成");
}

void loop() {
  WiFiClient client = server.available();
  
  if (client) {
    Serial.println("设备连接成功!");
    String currentLine = "";

    while (client.connected()) {
      if (client.available()) {
        currentLine = client.readStringUntil('\n');  // 读取直到换行符
        currentLine.trim();                          // 去掉两端的空白字符
        sscanf(currentLine.c_str(), "%d %d %d %d %d %d", &f1, &f2, &f3, &f4, &f5, &f6);
        ledcWrite(c1, f1);
        ledcWrite(c2, f2);
        ledcWrite(c3, f3);
        ledcWrite(c4, f4);
        ledcWrite(c5, f5);
        ledcWrite(c6, f6);
        currentLine = "";
      }
    }
  } 
}
